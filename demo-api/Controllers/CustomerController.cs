﻿using AutoMapper;
using Contracts;
using Entities;
using Entities.DTOs;
using Logger;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demo_api.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;

        public CustomerController(IUnitOfWork unit, ILoggerManager logger, IMapper mapper)
        {
            _unitOfWork = unit;
            _logger = logger;
            _mapper = mapper;
        }


        [HttpGet]
        public IActionResult GetCustomers()
        {

                var customers = _unitOfWork.Customer.GetAllCustomers(false);

                var customersDto = _mapper.Map<IEnumerable<CustomerDto>>(customers);

                return Ok(customersDto);
        }

    }
}
