﻿using AutoMapper;
using Contracts;
using Entities.DTOs;
using Entities.Models;
using Logger;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demo_api.Controllers
{
    [Route("api/Account/{id}")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;

        public AccountController(IUnitOfWork unit, ILoggerManager logger, IMapper mapper)
        {
            _unitOfWork = unit;
            _logger = logger;
            _mapper = mapper;
        }


        [HttpGet]
        public ActionResult<Account> GetAccount(Guid Id)
        {
                
            var account = _unitOfWork.Account.FindAccount(Id);
            var accountDto = new AccountDto();
            accountDto = _mapper.Map<AccountDto>(account);

            return Ok(accountDto);
        }
    }
}
