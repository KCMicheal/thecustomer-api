﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities
{
    public class Customer
    {
        [Key]
        public Guid Id { get; set; }
        [MaxLength(20)]
        [MinLength(4)]
        public string FirstName { get; set; }

        [MaxLength(20)]
        [MinLength(4)]
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Nationality { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool DefaultPassword { get; set; }
        public bool IsActive { get; set; }
    }
}
