﻿using Contracts;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private AppDbContext _context;
        private ICustomerRepository _customers;
        private IAccountRepository _accounts;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }

        public ICustomerRepository Customer { get { return _customers ??= _customers = new CustomerRepository(_context); } }
        public IAccountRepository Account { get { return _accounts ??= _accounts = new AccountRepository(_context); } }

        public void Save() => _context.SaveChanges();

    }
}
