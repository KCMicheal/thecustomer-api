﻿using Contracts;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(AppDbContext context) : base(context)
        {
        }

        public IEnumerable<Customer> GetAllCustomers(bool trackChanges) =>
            FindAll(trackChanges).OrderBy(c => c.Id).ToList();
    }
}
